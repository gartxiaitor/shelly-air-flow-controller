# Shelly Air Flow Controller
## Intro
Some fans/air renewal systems offer the possibility of regulating the air flow using a phisically connected rotatory regulator.

This regulator is usually connected via a RJ12 connector to the system. It works by closing the circuit between one of the wires (the one carrying the load) and the rest of the 6 wires of the RJ12 cable.

In the above example (pg. 40 https://www.siberzone.es/Media/Uploads/dlm_uploads/siber-manual-instalacion-vmc-df-sky-2017.pdf) the wire 6 carries the load, and is branched to wire 2, in order to let the system know about the regulator. 
* At rest position, the available load is redirected to wire 3. 
* At speed 1, it is not redirected anywhere. 
* At speed 2, it is redirected to wire 4. 
* And finally, at speed 3 it is redirected to wire 5. 

The **aim of this project** is to achieve that behaviour using Shelly dry contact relays such as Shelly 1, using as many of them as circuits to be closed, 3 in this case, and a Shelly Button to control that behaviour. Thus, **controlling the air flow by tapping a button**.

## Devices used
* Shelly 1 (x3)
* Shelly Button 1 (x1)

## Setup
1. Connect the 3 Shelly 1s loads and neutrals in paralallel, let them ready to connect to the PS.
2. Cut one of the ends of a RJ12 cable, and peal it exposing the 6 wires.
3. Identify each wire by ordering them the same way as they are ordered in the remaining connector. Test if connecting wires 6 and 2 triggers speed 1, and so on, based on the manufacturers specs.
4. Connect wires 2, 4 and 5 to each of the outputs(O) of the Shellyes, one by one.
5. Add 3 branches to wire 6, and connect them to the inputs of the shellyes (I), one by one.
6. Do NOT connect the RJ12 cable to the system until configuring the automations as explained in the next section of this document.
7. Connect the parallel Shelly 1 arrangement to PS.

## Config
1. After adding the Shelly 1s and button to the app, create an automation named ‘Rest’ triggered by a long push on the Shelly Button, and add 2 actions that switch off Shellyes 2 and 3, and then an action that turns on Shelly 1.
2. Repeat this process for ‘Speed 2’ and Speed 3’, triggering them by 2 and 3 taps respectively on the Shelly Button, first opening the circuit between the wire pairs that should remain disconnected at those speeds, and then closing the circuit between the wires that should remain connected at those speeds. 
3. Remember that in this case, according to the system manufacturer specs, speed 1 closes no circuit, so ‘Speed 1’ simply turns off all the Shelly 1s, and is trigger by 1 Shelly Button tap.

## Test
I strongly recommend testing the controller **BEFORE** connecting it to the system by measuring the conductivity of each of the wire pairs at each speed position, ensuring that the behaviour is the expected one.

1. With the controller disconnected from the system: 
2. Long press the Shelly Button so that the controller turns into rest position.
3. Connect the controller to the system, the air should be at rest speed.
4. Press once the Shelly 1 button, so that the controller turns into speed 1, the air flow should increase.
5. Test the rest of the speeds.





